/*global cordova,window,console*/
/**
 * An Utility plugin for Cordova
 *
 * Developed by thumbsupcoder
 */
var channel = require('cordova/channel'),cordova = require('cordova');
channel.createSticky('onCordovaUtilReady');
// Tell cordova channel to wait on the CordovaInfoReady event
channel.waitForInitialization('onCordovaUtilReady');
function Utility() {
    this.isAndroid = null;
    this.isIOS = null;
    this.isOnline = null;
    var self = this;
    document.addEventListener("online",function() {
      self.isOnline = true;
    }, false);
    document.addEventListener("offline",function () {
      self.isOnline = false;
    }, false);
    channel.onCordovaReady.subscribe(function() {
      if (cordova.platformId === 'android') {
          self.isAndroid = true;
      }
      else {
          self.isAndroid = false;
      }
      if (cordova.platformId === 'ios') {
          self.isIOS = true;
      }
      else {
          self.isIOS = false;
      }
      channel.initializationComplete('onCordovaUtilReady');
    });
}
Utility.prototype.encodeBase64String = function (successCallBack, failureCallBack, options) {
    options = options || {}
    cordova.exec(successCallBack,failureCallBack,"FJUtility","encodeBase64String",[options]);
}
Utility.prototype.clipboardCopy = function (successCallBack, failureCallBack, value) {
    if (typeof value === "undefined" || value === null) value = "";
  	cordova.exec(successCallBack, failureCallBack, "Clipboard", "copy", [value]);
}
Utility.prototype.clipboardPaste = function (successCallBack, failureCallBack) {
	  cordova.exec(successCallBack, failureCallBack, "Clipboard", "paste", []);
}
Utility.prototype.convertVideoToMpeg = function (successCallBack, failureCallBack, fileURL) {
    cordova.exec(successCallBack,failureCallBack,"FJUtility","convertVideoToMpeg",[fileURL]);
}
Utility.prototype.mimeTypeForFilePath = function (successCallBack, failureCallBack, fileURL) {
    cordova.exec(successCallBack,failureCallBack,"FJUtility","mimeTypeForFilePath",[fileURL]);
}
Utility.prototype.saveImageToPhotosAlbum = function (successCallBack, failureCallBack, options) {
    options = options || {}
    cordova.exec(successCallBack,failureCallBack,"FJUtility","saveImageToPhotosAlbum",[options]);
}
Utility.prototype.saveVideoToPhotosAlbum = function (successCallBack, failureCallBack, options) {
    options = options || {}
    cordova.exec(successCallBack,failureCallBack,"FJUtility","saveVideoToPhotosAlbum",[options]);
}
module.exports = new Utility()
