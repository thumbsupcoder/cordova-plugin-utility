cordova-utility
===================

Cordova Plugin For Helper Functions

## Installing the plugin

The plugin conforms to the Cordova plugin specification, it can be installed
using the Cordova / Phonegap command line interface.

    cordova plugin add cordova-plugin-utility

## Using the plugin

The plugin creates the object `plugins.utility` in `window` through which you can access the Utility functions and variables

Example - Check whether platform is Android or not:
```javascript
var isAndroid = plugins.utility.isAndroid;
```
```javascript
var isIOS = plugins.utility.isIOS;
```
## License
MIT
