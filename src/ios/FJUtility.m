//
//  FJUtility.m
//  freejobs
//
//  Created by thumbsupcoder on 21/09/16.
//
//

#import "FJUtility.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <objc/runtime.h>
#import <AssetsLibrary/AssetsLibrary.h>

@implementation FJUtility

- (void)encodeBase64String:(CDVInvokedUrlCommand *)command {

    if (command.arguments.count == 0) {
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
    }
    else {
        [self.commandDelegate runInBackground:^{
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Error generating base64 encoded string..."];
            NSDictionary *options = command.arguments[0];
            NSString *fileURL = options[@"fileURL"] ?: nil;
            if (fileURL) {
                NSData *fileData = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
                if (fileData == nil) {
                    fileData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:fileURL]];
                }
                if (fileData.length > 0) {
                    NSString *encodedBase64String = [fileData base64EncodedStringWithOptions:0];
                    if (encodedBase64String && encodedBase64String.length > 0) {
                        NSString *mimeType = [self MIMETypeForFileExtension:fileURL.pathExtension];
                        NSString *base64Format = [NSString stringWithFormat:@"data:%@;base64,%@",mimeType,encodedBase64String];
                        NSArray *mimeComponents = [mimeType componentsSeparatedByString:@"/"];
                        NSString *type = @"image";
                        if (mimeComponents.count > 0) {
                            type = mimeComponents[0];
                        }
                        NSDictionary *encodedResult = @{@"type":type,@"base64":base64Format};
                        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:encodedResult];
                    }
                }
            }
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }];
    }
}

- (NSString *)MIMETypeForFileExtension:(NSString *)fileExtension {
    NSString *UTI = (__bridge_transfer NSString *)UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)fileExtension, NULL);
    NSString *MIMEType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)UTI, kUTTagClassMIMEType);
    return MIMEType;
}

- (void)convertVideoToMpeg:(CDVInvokedUrlCommand *)command {

    __block CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Error Converting Video"];
    [self.commandDelegate runInBackground:^{
        NSString *videoPath = [[command argumentAtIndex:0 withDefault:@""] stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        NSURL *videoURL = [[NSURL alloc] initFileURLWithPath:videoPath];
        AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:videoURL options:nil];
        NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:avAsset];
        if ([compatiblePresets containsObject:AVAssetExportPresetLowQuality]) {
            AVMutableComposition *avComposition = [[AVMutableComposition alloc]init];
            AVAssetTrack *videoTrack = nil;
            AVAssetTrack *audioTrack = nil;
            CMTime insertionPoint = kCMTimeZero;
            NSError *error;
            if([[avAsset tracksWithMediaType:AVMediaTypeVideo] count] > 0) {
                videoTrack = [avAsset tracksWithMediaType:AVMediaTypeVideo][0];
            }
            if([[avAsset tracksWithMediaType:AVMediaTypeAudio] count] > 0) {
                audioTrack = [avAsset tracksWithMediaType:AVMediaTypeAudio][0];
            }
            AVMutableCompositionTrack *compositionVideoTrack = [avComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
            AVMutableCompositionTrack *compositionAudioTrack = [avComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            if (videoTrack != nil) {
                [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [avAsset duration]) ofTrack:videoTrack atTime:insertionPoint error:&error];
            }
            if (audioTrack != nil) {
                [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [avAsset duration]) ofTrack:audioTrack atTime:insertionPoint error:&error];
            }
            AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avComposition presetName:AVAssetExportPresetLowQuality];
            NSString *outputPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"capturedVideo.mp4"];
            NSURL *outputURL = [[NSURL alloc] initFileURLWithPath:outputPath];
            if ([[NSFileManager defaultManager]fileExistsAtPath:outputPath]) {
                [[NSFileManager defaultManager]removeItemAtPath:outputPath error:nil];
            }
            exportSession.outputURL = outputURL;
            exportSession.outputFileType = AVFileTypeMPEG4;
            exportSession.shouldOptimizeForNetworkUse = YES;
            [exportSession exportAsynchronouslyWithCompletionHandler:^{
                switch ([exportSession status])	{
                    case AVAssetExportSessionStatusCompleted:
                        NSLog(@"Converted Video path %@",outputPath);
                        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:outputPath];
                        break;
                    default:
                        break;
                }
                [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            }];
        }
        else {
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
    }];
}

- (void)mimeTypeForFilePath:(CDVInvokedUrlCommand *)command {

    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Error checking media type..."];
    NSString *filePath = [command argumentAtIndex:0 withDefault:nil];
    if (filePath) {
        NSString *MIMEType = [self MIMETypeForFileExtension:[filePath pathExtension]];
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:MIMEType];
    }
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)saveImageToPhotosAlbum:(CDVInvokedUrlCommand *)command {
    NSDictionary *options = command.arguments[0];
    __block NSString *sourceURL = options[@"url"];
    BOOL shouldRemoveItemOnSave = [options[@"shouldRemoveItemOnSave"] boolValue];
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:sourceURL]];
    [self.commandDelegate runInBackground:^{
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
            NSString *assetUrlString = assetURL.absoluteString;
            CDVPluginResult *result = error == NULL
            ? [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:assetUrlString]
            : [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:error.description];
            if (shouldRemoveItemOnSave) {
                NSError *error;
                sourceURL = [sourceURL stringByReplacingOccurrencesOfString:@"file://" withString:@""];
                [[NSFileManager defaultManager] removeItemAtPath:sourceURL error:&error];
            }
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }];
    }];
}

- (void)saveVideoToPhotosAlbum:(CDVInvokedUrlCommand *)command {
    NSDictionary *options = command.arguments[0];
    __block NSString *filePath = options[@"url"];
    NSString *sourcePath = [filePath stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    BOOL shouldRemoveItemOnSave = [options[@"shouldRemoveItemOnSave"] boolValue];
    NSURL *sourceURL = [NSURL fileURLWithPath:sourcePath];
    [self.commandDelegate runInBackground:^{
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:sourceURL]) {
            [library writeVideoAtPathToSavedPhotosAlbum:sourceURL completionBlock:^(NSURL *assetURL, NSError *error) {
                if (shouldRemoveItemOnSave) {
                    [[NSFileManager defaultManager] removeItemAtPath:sourcePath error:NULL];
                }
                NSString *assetUrlString = assetURL.absoluteString;
                CDVPluginResult *result = error == NULL
                ? [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:assetUrlString]
                : [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:error.description];
                [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            }];
        }
        else {
            CDVPluginResult *result = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:@"Incompatible format"];
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
    }];
}

@end

@implementation FJClipboard

- (void)copy:(CDVInvokedUrlCommand*)command {

    [self.commandDelegate runInBackground:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        NSString *text = [command argumentAtIndex:0 withDefault:@""];
        [pasteboard setValue:text forPasteboardType:@"public.text"];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:text];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)paste:(CDVInvokedUrlCommand*)command {

    [self.commandDelegate runInBackground:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        NSString *text = [pasteboard valueForPasteboardType:@"public.text"];
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:text];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

@end

#ifndef NULLSAFE_ENABLED
#define NULLSAFE_ENABLED 1
#endif

#pragma GCC diagnostic ignored "-Wgnu-conditional-omitted-operand"

@implementation NSNull (NullSafe)

#if NULLSAFE_ENABLED

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector {
    @synchronized([self class]) {
        //look up method signature
        NSMethodSignature *signature = [super methodSignatureForSelector:selector];
        if (!signature) {
            //not supported by NSNull, search other classes
            static NSMutableSet *classList = nil;
            static NSMutableDictionary *signatureCache = nil;
            if (signatureCache == nil) {
                classList = [[NSMutableSet alloc] init];
                signatureCache = [[NSMutableDictionary alloc] init];
                
                //get class list
                int numClasses = objc_getClassList(NULL, 0);
                Class *classes = (Class *)malloc(sizeof(Class) * (unsigned long)numClasses);
                numClasses = objc_getClassList(classes, numClasses);
                
                //add to list for checking
                NSMutableSet *excluded = [NSMutableSet set];
                for (int i = 0; i < numClasses; i++) {
                    //determine if class has a superclass
                    Class someClass = classes[i];
                    Class superclass = class_getSuperclass(someClass);
                    while (superclass) {
                        if (superclass == [NSObject class]) {
                            [classList addObject:someClass];
                            break;
                        }
                        [excluded addObject:NSStringFromClass(superclass)];
                        superclass = class_getSuperclass(superclass);
                    }
                }
                //remove all classes that have subclasses
                for (Class someClass in excluded) {
                    [classList removeObject:someClass];
                }
                //free class list
                free(classes);
            }
            //check implementation cache first
            NSString *selectorString = NSStringFromSelector(selector);
            signature = signatureCache[selectorString];
            if (!signature) {
                //find implementation
                for (Class someClass in classList) {
                    if ([someClass instancesRespondToSelector:selector]) {
                        signature = [someClass instanceMethodSignatureForSelector:selector];
                        break;
                    }
                }
                //cache for next time
                signatureCache[selectorString] = signature ?: [NSNull null];
            }
            else if ([signature isKindOfClass:[NSNull class]]) {
                signature = nil;
            }
        }
        return signature;
    }
}

- (void)forwardInvocation:(NSInvocation *)invocation {
    invocation.target = nil;
    [invocation invoke];
}

#endif

@end
