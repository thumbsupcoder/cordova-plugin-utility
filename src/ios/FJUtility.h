//
//  FJUtility.h
//  freejobs
//
//  Created by thumbsupcoder on 21/09/16.
//
//

#import <Cordova/CDVPlugin.h>
#import <AVFoundation/AVFoundation.h>

@interface FJUtility : CDVPlugin

- (void)encodeBase64String:(CDVInvokedUrlCommand *)command;

- (void)convertVideoToMpeg:(CDVInvokedUrlCommand *)command;

- (void)mimeTypeForFilePath:(CDVInvokedUrlCommand *)command;

- (void)saveImageToPhotosAlbum:(CDVInvokedUrlCommand *)command;

- (void)saveVideoToPhotosAlbum:(CDVInvokedUrlCommand *)command;

@end

@interface FJClipboard : CDVPlugin

- (void)copy:(CDVInvokedUrlCommand*)command;

- (void)paste:(CDVInvokedUrlCommand*)command;

@end

@interface NSNull (NullSafe)

@end
